# cmd-template

[![npm version](https://badge.fury.io/js/cmd-template.svg)](https://badge.fury.io/js/cmd-template)
[![build status](https://gitlab.com/sliv.gcp/cmd-template/badges/master/build.svg)](https://gitlab.com/sliv.gcp/cmd-template/commits/master)
[![coverage report](https://gitlab.com/sliv.gcp/cmd-template/badges/master/coverage.svg)](https://gitlab.com/sliv.gcp/cmd-template/commits/master)

## introduction

an intuitive shell utility using template literal.

## installation

```
npm i cmd-template
```

## usage


```typescript
import {sh, CmdTemplate} from 'cmd-template'

const opt1 = {
    follow: true,
    H: [
        "User-Agent: google-chrome",
        "x-api-key: xyz"
    ]
};
const url = "https://myserver/hello";
CmdTemplate`curl ${opt1} ${url}`;
// => String(curl --follow -H "User-Agent: google-chrome" -H "x-api-key: xyz" "https://myserver/hello")
// all the argument is automatically stringify.
```

```
// similarly the utility function sh
async function task () {    
    const [stdout, stderr] = await sh`curl ${opt1} ${url}`;
    
    // use node's exec from spawn_process
    // by default runs with {shell: process.env['SHELL'], print: true}
    // if more fine control need, please use spawn
    
    // "print" is a custom property for piping the exec's stdout, stderr to process's stdio

    // to override option 
    await sh.withOptions({print: false})`echo hello world`
    // => nothing will be printed
    
}
```