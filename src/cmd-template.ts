import { Interfaces } from './interface';
import { templateArgProcessor } from './opt-processor';

export const CmdTemplate: Interfaces.CmdTemplate = function CmdTemplate(
    str: TemplateStringsArray,
    ...args: any[]
) {return str.reduce((pr, ne, i) => pr + ne + templateArgProcessor(args[i]), '');};