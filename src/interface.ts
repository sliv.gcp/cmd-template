import { ExecOptions } from 'child_process';

export namespace Interfaces {
    export type StdoutStdErrPair = [string, string];

    export interface shOption extends ExecOptions {
        print?: boolean
    }

    export interface sh {
        withOption: (opt: shOption) => sh;

        (arr: TemplateStringsArray, ...args): Promise<StdoutStdErrPair>
    }

    export interface CmdTemplate {
        (arr: TemplateStringsArray, ...args: any[]): string
    }
}