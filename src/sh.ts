import { ChildProcess, exec } from 'child_process';
import { CmdTemplate } from './cmd-template';
import { assign } from './func';
import { Interfaces } from './interface';
import StdoutStdErrPair = Interfaces.StdoutStdErrPair;

const defaultExecOptions = {
    shell: process.env['SHELL'],
    encoding: 'utf-8',
    print: true,
};

function printChildProcess(proc: ChildProcess) {
    proc.stderr.pipe(process.stderr);
    proc.stdout.pipe(process.stdout);
}

async function shFactory(a1, ...args) {
    const { execOptions = defaultExecOptions } = this;
    const { print, ...option } = execOptions;
    return new Promise<StdoutStdErrPair>((r, d) => {
        const childProcess = exec(CmdTemplate(a1, ...args), option, (err, stdout, stderr) => {
            if (err) return d(err);
            return r([String(stdout), String(stderr)]);
        });
        if (print) printChildProcess(childProcess);
    });

}

export const sh: Interfaces.sh = assign(shFactory, {
    withOption(execOptions) {
        return sh.bind({ execOptions });
    },
});