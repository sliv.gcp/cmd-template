import { sh } from '../sh';

describe('sh test', async () => {
    const mockWrite = jest.fn();
    const originalWrite = process.stdout.write;
    beforeEach(() => {
        process.stdout['write'] = mockWrite;
    });

    afterEach(() => {
        process.stdout['write'] = originalWrite;
        mockWrite.mockClear();
    });

    test('base', async () => {
        const [lsResult] = await sh`echo ${'working'}`;
        expect(typeof lsResult === 'string').toBeTruthy();
        expect(mockWrite).toBeCalled();
        expect(mockWrite.mock.calls[0][0]).toMatch(/working/);
    });

    test('no print', async () => {
        const [lsResult] = await sh.withOption({ print: false })`echo ${'noprint'}`;
        expect(typeof lsResult === 'string').toBeTruthy();
        expect(lsResult).toMatch('noprint');
        expect(mockWrite).not.toBeCalled();
    });
});