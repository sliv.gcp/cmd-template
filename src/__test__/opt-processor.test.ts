import { CmdTemplate } from '../cmd-template';
import { optProcessor } from '../opt-processor';

function JestFnUnitTestHelper(fn, operator = 'toEqual') {
    return (...args) => v => expect(fn(...args))[operator](v);
}

describe('opt-processor test', () => {
    const f = JestFnUnitTestHelper(optProcessor);
    test('sample cases', () => {
        f(['-b', true])('-b');
        f(['-b', false])('');
        f(['-b', 'yes'])('-b "yes"');
        f(['--hello', 'world'])('--hello "world"');
        f(['--hello', true])('--hello');
        f(['--hello', false])('');
        f(['-H', ['x-engine: express', 'user-engine: chrome']])(
            '-H "x-engine: express" -H "user-engine: chrome"',
        );
    });
});

describe('cmd template test', () => {
    function Curl(url: string, opt: { follow: boolean, H: string[] }) {
        return CmdTemplate`curl ${opt} ${url}`;
    }

    const c1 = 'curl --follow -H "x-engine: express" -H "user-engine: chrome" "https://google.com"';
    expect(Curl('https://google.com', { follow: true, H: ['x-engine: express', 'user-engine: chrome'] }))
        .toEqual(c1);
});