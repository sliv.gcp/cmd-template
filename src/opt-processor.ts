import { matchHarness } from 'ramda-match';
import { adjust, is, toPairs } from './func';

// @internal
export const flagProcessor = (s: string) => (s.length == 1 ? '-' : '--') + s;
// @internal
export const optProcessor = matchHarness<String>(m => {
    const whenVal = f => m(([k, v]) => f(v));
    const whenArrVal = whenVal(Array.isArray);
    const whenFalseVal = whenVal(v => !v);
    const whenTrueVal = whenVal(v => is.bool(v) && v);
    const whenStrVal = whenVal(is.str);
    const whenElse = whenVal(() => true);
    whenArrVal(([k, v]) => v.map(s => optProcessor([k, s])).join(' '));
    whenFalseVal(() => '');
    whenTrueVal(([k]) => k);
    whenStrVal(([k, v]) => `${k} ${JSON.stringify(v)}`);
    whenElse(([k, v]) => `${k} ${v}`);
});
// @internal
export const templateArgProcessor = matchHarness<String>(m => {
    m(s => s === undefined)(() => '');
    m(is.obj)(o => toPairs(o)
        .map(adjust(0)(flagProcessor))
        .map(optProcessor)
        .filter((i: string) => i.length > 0)
        .join(' '));
    m(is.str)(JSON.stringify);
    m(() => true)(i => i);
});