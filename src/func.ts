// @internal
export function isType(type: string) {return s => typeof s === type;}

// @internal
export const is = {
    arr: Array.isArray,
    bool: isType('boolean'),
    str: isType('string'),
    obj: isType('object'),
};

// @internal
export function toPairs(o: object) {
    return Object.keys(o).map(k => [k, o[k]]);
}

// @internal
export function adjust(i: number) {
    return f => a => {
        const o = [...a];
        o[i] = f(o[i]);
        return o;
    };
}

// @internal
export const assign = Object.assign;